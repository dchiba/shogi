package koma

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestType_CanNari(t *testing.T) {
	samples := []struct {
		Type         Type
		ExpectedBool bool
	}{
		{GYOKU, false}, {HISYA, true}, {KAKU, true}, {KIN, false}, {GIN, true}, {KEI, true}, {KYOU, true}, {FU, true},
		{RYU, false}, {UMA, false}, {NARIGIN, false}, {NARIKEI, false}, {NARIKYOU, false}, {TOKIN, false},
		{ZOU, false}, {KIRIN, false},
	}
	for _, s := range samples {
		assert.Equal(t, s.ExpectedBool, s.Type.CanNari())
	}
}
