package koma

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestNew(t *testing.T) {
	k := New(2, HISYA)
	assert.Equal(t, k.ID, uint8(2))
	assert.Equal(t, k.Type, HISYA)
}
