package koma

type MoveType uint8

const (
	_ MoveType = iota
	Front
	Back
	Right
	Left
	FrontRight
	FrontLeft
	BackRight
	BackLeft
	FrontStraight
	BackStraight
	RightStraight
	LeftStraight
	FrontRightStraight
	FrontLeftStraight
	BackRightStraight
	BackLeftStraight
	FrontFrontRightJump
	FrontFrontLeftJump
)

type MoveTypes []MoveType

var KomaMoveType map[Type]MoveTypes = map[Type]MoveTypes{
	GYOKU:    MoveTypes{Front, Back, Right, Left, FrontRight, FrontLeft, BackRight, BackLeft},
	HISYA:    MoveTypes{FrontStraight, BackStraight, RightStraight, LeftStraight},
	KAKU:     MoveTypes{FrontRightStraight, FrontLeftStraight, BackRightStraight, BackLeftStraight},
	KIN:      MoveTypes{Front, Back, Right, Left, FrontRight, FrontLeft},
	GIN:      MoveTypes{Front, FrontRight, FrontLeft, BackRight, BackLeft},
	KEI:      MoveTypes{FrontFrontRightJump, FrontFrontLeftJump},
	KYOU:     MoveTypes{FrontStraight},
	FU:       MoveTypes{Front},
	RYU:      MoveTypes{FrontRight, FrontLeft, BackRight, BackLeft, FrontStraight, BackStraight, RightStraight, LeftStraight},
	UMA:      MoveTypes{Front, Back, Right, Left, FrontRightStraight, FrontLeftStraight, BackRightStraight, BackLeftStraight},
	NARIGIN:  MoveTypes{Front, Back, Right, Left, FrontRight, FrontLeft},
	NARIKEI:  MoveTypes{Front, Back, Right, Left, FrontRight, FrontLeft},
	NARIKYOU: MoveTypes{Front, Back, Right, Left, FrontRight, FrontLeft},
	TOKIN:    MoveTypes{Front, Back, Right, Left, FrontRight, FrontLeft},
	ZOU:      MoveTypes{Front, Back, Right, Left},
	KIRIN:    MoveTypes{FrontRight, FrontLeft, BackRight, BackLeft},
}
