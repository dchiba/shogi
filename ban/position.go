package ban

import (
	. "bitbucket.org/dchiba/shogi/util"
	"errors"
	"fmt"
)

type Position struct {
	X, Y int
}

type Positions []*Position

func (p *Position) String() string {
	return fmt.Sprintf("{X:%d Y:%d}", p.X, p.Y)
}

func (b *Ban) NewPosition(x, y int) (*Position, error) {
	if x < 0 || y < 0 || x >= b.Width || y >= b.Height {
		return nil, errors.New("invalid arguments")
	}
	return &Position{x, y}, nil
}

func (b *Ban) Front(p *Position, sente bool) (*Position, error) {
	return b.NewPosition(p.X, Ternary(sente, p.Y-1, p.Y+1).(int))
}

func (b *Ban) Back(p *Position, sente bool) (*Position, error) {
	return b.NewPosition(p.X, Ternary(sente, p.Y+1, p.Y-1).(int))
}

func (b *Ban) Right(p *Position, sente bool) (*Position, error) {
	return b.NewPosition(Ternary(sente, p.X-1, p.X+1).(int), p.Y)
}

func (b *Ban) Left(p *Position, sente bool) (*Position, error) {
	return b.NewPosition(Ternary(sente, p.X+1, p.X-1).(int), p.Y)
}

func (b *Ban) FrontRight(p *Position, sente bool) (*Position, error) {
	return b.NewPosition(Ternary(sente, p.X-1, p.X+1).(int), Ternary(sente, p.Y-1, p.Y+1).(int))
}

func (b *Ban) FrontLeft(p *Position, sente bool) (*Position, error) {
	return b.NewPosition(Ternary(sente, p.X+1, p.X-1).(int), Ternary(sente, p.Y-1, p.Y+1).(int))
}

func (b *Ban) BackRight(p *Position, sente bool) (*Position, error) {
	return b.NewPosition(Ternary(sente, p.X-1, p.X+1).(int), Ternary(sente, p.Y+1, p.Y-1).(int))
}

func (b *Ban) BackLeft(p *Position, sente bool) (*Position, error) {
	return b.NewPosition(Ternary(sente, p.X+1, p.X-1).(int), Ternary(sente, p.Y+1, p.Y-1).(int))
}

func (p *Position) Equal(q *Position) bool {
	return q != nil && p.X == q.X && p.Y == q.Y
}

func (ps Positions) Contain(q *Position) (b bool) {
	b = false
	for _, p := range ps {
		if p.Equal(q) {
			b = true
			break
		}
	}
	return
}
