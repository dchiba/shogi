package ban

import (
	"bitbucket.org/dchiba/shogi/koma"
	"bitbucket.org/dchiba/shogi/player"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestNewKoma(t *testing.T) {
	k := NewKoma(1, koma.GYOKU, player.New(1, true))
	assert.NotNil(t, k)
}

func TestKoma_MovableTime(t *testing.T) {
	k := NewKoma(1, koma.GYOKU, player.New(1, true))
	assert.NotNil(t, k.MovableTime())
}
