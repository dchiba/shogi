package ban

import (
	"bitbucket.org/dchiba/shogi/koma"
	"bitbucket.org/dchiba/shogi/player"
	"errors"
	"strings"
)

const (
	DefaultWidth      = 9
	DefaultHeight     = 9
	DefaultNariHeight = 3

	MiniWidth      = 5
	MiniHeight     = 6
	MiniNariHeight = 2

	MicroWidth      = 3
	MicroHeight     = 4
	MicroNariHeight = 1
)

type Ban struct {
	Width      int
	Height     int
	NariHeight int
	Table      [][]*Koma
	MaxID      uint8
}

func NewDefaultBan() *Ban {
	return NewBan(DefaultWidth, DefaultHeight, DefaultNariHeight)
}

func NewMiniBan() *Ban {
	return NewBan(MiniWidth, MiniHeight, MiniNariHeight)
}

func NewMicroBan() *Ban {
	return NewBan(MicroWidth, MicroHeight, MicroNariHeight)
}

func NewBan(width, height, nariHeight int) *Ban {
	t := [][]*Koma{}
	for i := 0; i < height; i++ {
		t = append(t, []*Koma{})
		for j := 0; j < width; j++ {
			t[i] = append(t[i], nil)
		}
	}
	return &Ban{
		Width:      width,
		Height:     height,
		NariHeight: nariHeight,
		Table:      t,
	}
}

func (b *Ban) String() string {
	ary := []string{}
	for _, row := range b.Table {
		for _, col := range row {
			if col == nil {
				ary = append(ary, "nil")
			} else {
				ary = append(ary, col.String())
			}
		}
		ary = append(ary, "\n")
	}
	return strings.Join(ary, ", ")
}

func (b *Ban) SetKoma(pos *Position, t koma.Type, p *player.Player) (k *Koma) {
	b.MaxID++
	k = NewKoma(b.MaxID, t, p)
	b.Table[pos.Y][pos.X] = k
	return
}

func (b *Ban) GetKoma(pos *Position) (k *Koma, e error) {
	k = b.Table[pos.Y][pos.X]
	if k == nil {
		e = errors.New("Koma is not exist")
	}
	return
}

func (b *Ban) MoveKoma(from *Position, to *Position) (got *Koma, e error) {
	fromKoma, e := b.GetKoma(from)
	if e != nil {
		return nil, e
	}

	toKoma, e := b.GetKoma(to)
	if toKoma != nil && fromKoma.Player.ID == toKoma.Player.ID {
		return nil, errors.New("Target is mine")
	}

	b.Table[from.Y][from.X] = nil
	b.Table[to.Y][to.X] = fromKoma
	fromKoma.UpdateLastMoveUnixtime()

	return toKoma, nil
}

func (b *Ban) PutKoma(p *player.Player, k *koma.Koma, pos *Position) (*Koma, error) {
	_, e := b.GetKoma(pos)
	if e == nil {
		return nil, errors.New("Koma is exist")
	}
	newKoma := NewKomaFromKoma(*k, p)
	b.Table[pos.Y][pos.X] = newKoma
	return newKoma, nil
}
