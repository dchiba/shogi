package player

import (
	"bitbucket.org/dchiba/shogi/koma"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestNew(t *testing.T) {
	p := New(1, true)
	assert.NotNil(t, p)
}

func TestPlayer_AppendTegoma(t *testing.T) {
	p := New(1, true)
	k := koma.New(1, koma.FU)

	c, e := p.AppendTegoma(k)
	assert.Nil(t, e, "手駒に追加できた")
	assert.Equal(t, c.ID, k.ID)
	assert.Equal(t, len(p.Tegoma), 1)
	assert.Equal(t, p.Tegoma[0].Type, koma.FU)

	_, e = p.AppendTegoma(k)
	assert.NotNil(t, e, "重複を許さない")

	k = koma.New(2, koma.RYU)
	c, e = p.AppendTegoma(k)
	assert.Nil(t, e, "手駒に追加できた")
	assert.Equal(t, c.ID, k.ID)
	assert.Equal(t, len(p.Tegoma), 2)
	assert.Equal(t, p.Tegoma[1].Type, koma.HISYA, "成り駒はもとの種別に戻される")
}

func TestPlayer_RemoveTegoma(t *testing.T) {
	p := New(1, true)
	k1 := koma.New(1, koma.FU)
	k2 := koma.New(2, koma.KIN)
	p.AppendTegoma(k1)

	_, e := p.RemoveTegoma(k2)
	assert.NotNil(t, e, "存在しない駒は削除できない")

	k, e := p.RemoveTegoma(k1)
	assert.Nil(t, e, "正常に削除できる")
	assert.Equal(t, k.ID, k1.ID, "同一オブジェクトが返ってくる")
	assert.Equal(t, len(p.Tegoma), 0, "正常に削除されている")
}
