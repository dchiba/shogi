package teaiwari

import (
	"bitbucket.org/dchiba/shogi/koma"
)

var Micro Teaiwari = Teaiwari{
	Position{0, 0, koma.KIRIN, true},
	Position{1, 0, koma.GYOKU, true},
	Position{2, 0, koma.ZOU, true},

	Position{1, 1, koma.FU, true},

	Position{1, 2, koma.FU, false},

	Position{0, 3, koma.ZOU, false},
	Position{1, 3, koma.GYOKU, false},
	Position{2, 3, koma.KIRIN, false},
}
