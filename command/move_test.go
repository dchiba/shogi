package command

import (
	"bitbucket.org/dchiba/shogi/apptime"
	"bitbucket.org/dchiba/shogi/ban"
	"bitbucket.org/dchiba/shogi/koma"
	"bitbucket.org/dchiba/shogi/player"
	"bitbucket.org/dchiba/shogi/teaiwari"
	"errors"
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

func TestMove(t *testing.T) {
	assert := assert.New(t)
	b := ban.NewDefaultBan()
	p1 := player.New(1, true)
	p2 := player.New(2, false)

	Arrange(p1, p2, b, teaiwari.Teaiwari{
		teaiwari.Position{4, 4, koma.FU, true},
		teaiwari.Position{4, 3, koma.FU, false},
	})

	moved, got, captured, e := Move(p1, b, &ban.Position{4, 3}, &ban.Position{4, 4}, false)
	assert.NotNil(e, "移動できない")

	moved, got, captured, e = Move(p1, b, &ban.Position{4, 4}, &ban.Position{4, 3}, false)
	assert.Equal(e, errors.New("Koma is waiting time"))

	apptime.SetTime(time.Now().Add(1 * time.Minute)) // 駒の移動待ち時間のために時間をすすめる
	moved, got, captured, e = Move(p1, b, &ban.Position{4, 4}, &ban.Position{4, 3}, false)
	assert.Nil(e, "移動できる")
	assert.Equal(moved.Type, koma.FU, "なってない")
	assert.Equal(got.Type, koma.FU, "歩をゲット")
	assert.Equal(captured.Type, koma.FU, "歩をゲット")
	assert.Equal(len(p1.Tegoma), 1, "手駒に歩が追加されている")

	apptime.SetTime(time.Now().Add(2 * time.Minute)) // 駒の移動待ち時間のために時間をすすめる
	moved, got, captured, e = Move(p1, b, &ban.Position{4, 3}, &ban.Position{4, 2}, true)
	assert.Nil(e, "移動できる")
	assert.Equal(moved.Type, koma.TOKIN, "なっている")
	assert.Nil(got, "何もゲットできない")
}
