package command

import (
	"bitbucket.org/dchiba/shogi/ban"
	. "bitbucket.org/dchiba/shogi/koma"
	"bitbucket.org/dchiba/shogi/player"
	"bitbucket.org/dchiba/shogi/teaiwari"
	"fmt"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestArrange(t *testing.T) {
	uwate := player.New(1, true)
	shitate := player.New(2, false)
	b := ban.NewDefaultBan()
	tc := teaiwari.Teaiwari{
		teaiwari.Position{4, 0, GYOKU, true},
		teaiwari.Position{4, 8, GYOKU, false},
	}
	Arrange(uwate, shitate, b, tc)

	assert.Equal(t, b.MaxID, uint8(2), "駒が2枚作成されている")

	var k *ban.Koma
	var e error

	k, e = b.GetKoma(&ban.Position{4, 0})
	assert.Nil(t, e, "駒が存在する")
	assert.Equal(t, k.Type, GYOKU, "上手側の玉がある")
	assert.Equal(t, k.Player, uwate, "上手側の玉の所有者が上手")

	k, e = b.GetKoma(&ban.Position{4, 8})
	assert.Nil(t, e, "駒が存在する")
	assert.Equal(t, k.Type, GYOKU, "下手側の玉がある")
	assert.Equal(t, k.Player, shitate, "下手側の玉の所有者が上手")
}

func TestArrange_Hirate(t *testing.T) {
	uwate := player.New(1, true)
	shitate := player.New(2, false)
	b := ban.NewDefaultBan()
	Arrange(uwate, shitate, b, teaiwari.Hirate)

	t.Log(b)

	assert.Equal(t, b.MaxID, uint8(40), "40枚配置されている")

	types := [][]Type{
		{KYOU, KEI, GIN, KIN, GYOKU, KIN, GIN, KEI, KYOU},
		{0, KAKU, 0, 0, 0, 0, 0, HISYA, 0},
		{FU, FU, FU, FU, FU, FU, FU, FU, FU},
		{0, 0, 0, 0, 0, 0, 0, 0, 0},
		{0, 0, 0, 0, 0, 0, 0, 0, 0},
		{0, 0, 0, 0, 0, 0, 0, 0, 0},
		{FU, FU, FU, FU, FU, FU, FU, FU, FU},
		{0, HISYA, 0, 0, 0, 0, 0, KAKU, 0},
		{KYOU, KEI, GIN, KIN, GYOKU, KIN, GIN, KEI, KYOU},
	}
	for y, row := range types {
		for x, kt := range row {
			k, e := b.GetKoma(&ban.Position{x, y})
			if kt == 0 {
				assert.NotNil(t, e, fmt.Sprintf("X:%d, Y:%d に駒が存在しない", x, y))
			} else {
				assert.Nil(t, e, fmt.Sprintf("X:%d, Y:%d に駒が存在する", x, y))
				assert.Equal(t, k.Type, kt, fmt.Sprintf("X:%d, Y:%d の駒は %s", x, y, kt.Label()))
			}
		}
	}
}

func TestArrange_Mini(t *testing.T) {
	uwate := player.New(1, true)
	shitate := player.New(2, false)
	b := ban.NewMiniBan()
	Arrange(uwate, shitate, b, teaiwari.Mini)

	t.Log(b)

	assert.Equal(t, b.MaxID, uint8(16), "16枚配置されている")

	types := [][]Type{
		{GIN, KIN, GYOKU, KIN, GIN},
		{0, 0, 0, 0, 0},
		{0, FU, FU, FU, 0},
		{0, FU, FU, FU, 0},
		{0, 0, 0, 0, 0},
		{GIN, KIN, GYOKU, KIN, GIN},
	}
	for y, row := range types {
		for x, kt := range row {
			k, e := b.GetKoma(&ban.Position{x, y})
			if kt == 0 {
				assert.NotNil(t, e, fmt.Sprintf("X:%d, Y:%d に駒が存在しない", x, y))
			} else {
				assert.Nil(t, e, fmt.Sprintf("X:%d, Y:%d に駒が存在する", x, y))
				assert.Equal(t, k.Type, kt, fmt.Sprintf("X:%d, Y:%d の駒は %s", x, y, kt.Label()))
			}
		}
	}
}

func TestArrange_Micro(t *testing.T) {
	uwate := player.New(1, true)
	shitate := player.New(2, false)
	b := ban.NewMicroBan()
	Arrange(uwate, shitate, b, teaiwari.Micro)

	t.Log(b)

	assert.Equal(t, b.MaxID, uint8(8), "8枚配置されている")

	types := [][]Type{
		{KIRIN, GYOKU, ZOU},
		{0, FU, 0},
		{0, FU, 0},
		{ZOU, GYOKU, KIRIN},
	}
	for y, row := range types {
		for x, kt := range row {
			k, e := b.GetKoma(&ban.Position{x, y})
			if kt == 0 {
				assert.NotNil(t, e, fmt.Sprintf("X:%d, Y:%d に駒が存在しない", x, y))
			} else {
				assert.Nil(t, e, fmt.Sprintf("X:%d, Y:%d に駒が存在する", x, y))
				assert.Equal(t, k.Type, kt, fmt.Sprintf("X:%d, Y:%d の駒は %s", x, y, kt.Label()))
			}
		}
	}
}
