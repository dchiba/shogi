package command

import (
	"bitbucket.org/dchiba/shogi/ban"
	"bitbucket.org/dchiba/shogi/koma"
	"bitbucket.org/dchiba/shogi/player"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestPutKoma(t *testing.T) {
	b := ban.NewDefaultBan()
	p := player.New(1, true)

	komaid := uint8(1)
	p.AppendTegoma(koma.New(komaid, koma.FU))
	assert.Equal(t, len(p.Tegoma), 1, "手駒がある")

	pos := &ban.Position{4, 4}
	bk, e := PutKoma(p, b, p.Tegoma[0], pos)
	assert.Nil(t, e, "駒を打てた")
	assert.Equal(t, len(p.Tegoma), 0, "手駒が減っている")
	assert.Equal(t, bk.ID, komaid, "IDが同一")

	k, e := b.GetKoma(pos)
	assert.Nil(t, e, "打った駒が取得できた")
	assert.Equal(t, k.ID, uint8(1), "打った駒が手駒と同一だった")
}
