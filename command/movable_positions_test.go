package command

import (
	"bitbucket.org/dchiba/shogi/ban"
	"bitbucket.org/dchiba/shogi/koma"
	"bitbucket.org/dchiba/shogi/player"
	"fmt"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestMovablePositions(t *testing.T) {
	b := ban.NewDefaultBan()
	p1 := player.New(1, false)

	pos, _ := b.NewPosition(4, 4)
	k := b.SetKoma(pos, koma.FU, p1)

	sample := []struct {
		Type      koma.Type
		Positions MovePositions
	}{
		{koma.GYOKU, MovePositions{
			&MovePosition{&ban.Position{4, 5}, false}, // 前
			&MovePosition{&ban.Position{4, 3}, false}, // 後
			&MovePosition{&ban.Position{5, 4}, false}, // 右
			&MovePosition{&ban.Position{3, 4}, false}, // 左
			&MovePosition{&ban.Position{5, 5}, false}, // 右前
			&MovePosition{&ban.Position{3, 5}, false}, // 左前
			&MovePosition{&ban.Position{5, 3}, false}, // 右後
			&MovePosition{&ban.Position{3, 3}, false}, // 左後
		}},
		{koma.HISYA, MovePositions{
			&MovePosition{&ban.Position{4, 5}, false}, &MovePosition{&ban.Position{4, 6}, true}, &MovePosition{&ban.Position{4, 7}, true}, &MovePosition{&ban.Position{4, 8}, true},
			&MovePosition{&ban.Position{4, 3}, false}, &MovePosition{&ban.Position{4, 2}, false}, &MovePosition{&ban.Position{4, 1}, false}, &MovePosition{&ban.Position{4, 0}, false},
			&MovePosition{&ban.Position{5, 4}, false}, &MovePosition{&ban.Position{6, 4}, false}, &MovePosition{&ban.Position{7, 4}, false}, &MovePosition{&ban.Position{8, 4}, false},
			&MovePosition{&ban.Position{3, 4}, false}, &MovePosition{&ban.Position{2, 4}, false}, &MovePosition{&ban.Position{1, 4}, false}, &MovePosition{&ban.Position{0, 4}, false},
		}},
		{koma.KAKU, MovePositions{
			&MovePosition{&ban.Position{5, 5}, false}, &MovePosition{&ban.Position{6, 6}, true}, &MovePosition{&ban.Position{7, 7}, true}, &MovePosition{&ban.Position{8, 8}, true},
			&MovePosition{&ban.Position{3, 3}, false}, &MovePosition{&ban.Position{2, 2}, false}, &MovePosition{&ban.Position{1, 1}, false}, &MovePosition{&ban.Position{0, 0}, false},
			&MovePosition{&ban.Position{5, 3}, false}, &MovePosition{&ban.Position{6, 2}, false}, &MovePosition{&ban.Position{7, 1}, false}, &MovePosition{&ban.Position{8, 0}, false},
			&MovePosition{&ban.Position{3, 5}, false}, &MovePosition{&ban.Position{2, 6}, true}, &MovePosition{&ban.Position{1, 7}, true}, &MovePosition{&ban.Position{0, 8}, true},
		}},
		{koma.KIN, MovePositions{
			&MovePosition{&ban.Position{4, 5}, false},
			&MovePosition{&ban.Position{4, 3}, false},
			&MovePosition{&ban.Position{5, 4}, false},
			&MovePosition{&ban.Position{3, 4}, false},
			&MovePosition{&ban.Position{5, 5}, false},
			&MovePosition{&ban.Position{3, 5}, false},
		}},
		{koma.GIN, MovePositions{
			&MovePosition{&ban.Position{4, 5}, false},
			&MovePosition{&ban.Position{5, 5}, false},
			&MovePosition{&ban.Position{3, 5}, false},
			&MovePosition{&ban.Position{5, 3}, false},
			&MovePosition{&ban.Position{3, 3}, false},
		}},
		{koma.KEI, MovePositions{
			&MovePosition{&ban.Position{5, 6}, true}, // 右前
			&MovePosition{&ban.Position{3, 6}, true}, // 左前
		}},
		{koma.KYOU, MovePositions{
			&MovePosition{&ban.Position{4, 5}, false}, &MovePosition{&ban.Position{4, 6}, true}, &MovePosition{&ban.Position{4, 7}, true}, &MovePosition{&ban.Position{4, 8}, true},
		}},
		{koma.FU, MovePositions{
			&MovePosition{&ban.Position{4, 5}, false},
		}},
		{koma.RYU, MovePositions{
			&MovePosition{&ban.Position{4, 5}, false}, &MovePosition{&ban.Position{4, 6}, false}, &MovePosition{&ban.Position{4, 7}, false}, &MovePosition{&ban.Position{4, 8}, false},
			&MovePosition{&ban.Position{4, 3}, false}, &MovePosition{&ban.Position{4, 2}, false}, &MovePosition{&ban.Position{4, 1}, false}, &MovePosition{&ban.Position{4, 0}, false},
			&MovePosition{&ban.Position{5, 4}, false}, &MovePosition{&ban.Position{6, 4}, false}, &MovePosition{&ban.Position{7, 4}, false}, &MovePosition{&ban.Position{8, 4}, false},
			&MovePosition{&ban.Position{3, 4}, false}, &MovePosition{&ban.Position{2, 4}, false}, &MovePosition{&ban.Position{1, 4}, false}, &MovePosition{&ban.Position{0, 4}, false},
			&MovePosition{&ban.Position{5, 5}, false}, // 右前
			&MovePosition{&ban.Position{3, 5}, false}, // 左前
			&MovePosition{&ban.Position{5, 3}, false}, // 右後
			&MovePosition{&ban.Position{3, 3}, false}, // 左後
		}},
		{koma.UMA, MovePositions{
			&MovePosition{&ban.Position{5, 5}, false}, &MovePosition{&ban.Position{6, 6}, false}, &MovePosition{&ban.Position{7, 7}, false}, &MovePosition{&ban.Position{8, 8}, false},
			&MovePosition{&ban.Position{3, 3}, false}, &MovePosition{&ban.Position{2, 2}, false}, &MovePosition{&ban.Position{1, 1}, false}, &MovePosition{&ban.Position{0, 0}, false},
			&MovePosition{&ban.Position{5, 3}, false}, &MovePosition{&ban.Position{6, 2}, false}, &MovePosition{&ban.Position{7, 1}, false}, &MovePosition{&ban.Position{8, 0}, false},
			&MovePosition{&ban.Position{3, 5}, false}, &MovePosition{&ban.Position{2, 6}, false}, &MovePosition{&ban.Position{1, 7}, false}, &MovePosition{&ban.Position{0, 8}, false},
			&MovePosition{&ban.Position{4, 5}, false}, // 前
			&MovePosition{&ban.Position{4, 3}, false}, // 後
			&MovePosition{&ban.Position{5, 4}, false}, // 右
			&MovePosition{&ban.Position{3, 4}, false}, // 左
		}},
		{koma.NARIGIN, MovePositions{
			&MovePosition{&ban.Position{4, 5}, false},
			&MovePosition{&ban.Position{4, 3}, false},
			&MovePosition{&ban.Position{5, 4}, false},
			&MovePosition{&ban.Position{3, 4}, false},
			&MovePosition{&ban.Position{5, 5}, false},
			&MovePosition{&ban.Position{3, 5}, false},
		}},
		{koma.NARIKEI, MovePositions{
			&MovePosition{&ban.Position{4, 5}, false},
			&MovePosition{&ban.Position{4, 3}, false},
			&MovePosition{&ban.Position{5, 4}, false},
			&MovePosition{&ban.Position{3, 4}, false},
			&MovePosition{&ban.Position{5, 5}, false},
			&MovePosition{&ban.Position{3, 5}, false},
		}},
		{koma.NARIKYOU, MovePositions{
			&MovePosition{&ban.Position{4, 5}, false},
			&MovePosition{&ban.Position{4, 3}, false},
			&MovePosition{&ban.Position{5, 4}, false},
			&MovePosition{&ban.Position{3, 4}, false},
			&MovePosition{&ban.Position{5, 5}, false},
			&MovePosition{&ban.Position{3, 5}, false},
		}},
		{koma.TOKIN, MovePositions{
			&MovePosition{&ban.Position{4, 5}, false},
			&MovePosition{&ban.Position{4, 3}, false},
			&MovePosition{&ban.Position{5, 4}, false},
			&MovePosition{&ban.Position{3, 4}, false},
			&MovePosition{&ban.Position{5, 5}, false},
			&MovePosition{&ban.Position{3, 5}, false},
		}},
	}
	for _, s := range sample {
		k.Type = s.Type
		ps, e := MovablePositions(p1, b, pos)
		assert.Nil(t, e)
		assert.Equal(t, len(ps), len(s.Positions))
		for _, p := range ps {
			b := false
			index := -1
			for i, q := range s.Positions {
				if p.Position.Equal(q.Position) {
					b = true
					index = i
					break
				}
			}
			if assert.True(t, b, fmt.Sprintf("%s の移動先候補として %s が存在する", s.Type.Label(), p.String())) {
				assert.Equal(t, s.Positions[index].Nari, p.Nari, fmt.Sprintf("%s の %s の成判定が正しい", s.Type.Label(), p.String()))
			} else {
				t.Log(ps)
				t.Log(s.Positions)
			}
		}
	}
}

func TestMovablePositions2(t *testing.T) {
	b := ban.NewMiniBan()
	p1 := player.New(1, false)

	pos, _ := b.NewPosition(2, 3)
	b.SetKoma(pos, koma.FU, p1)

	ps, e := MovablePositions(p1, b, pos)
	if assert.Nil(t, e, "MiniMode で後手の成りテスト") {
		assert.Equal(t, len(ps), 1)
		assert.Equal(t, ps[0].Position.Y, 4)
		assert.True(t, ps[0].Nari, "MiniMode で後手だと Y:4 から成れる")
	}

	p2 := player.New(2, true)

	pos2, _ := b.NewPosition(2, 2)
	b.SetKoma(pos2, koma.FU, p2)
	ps2, e := MovablePositions(p2, b, pos2)
	if assert.Nil(t, e, "MiniMode で先手の成テスト") {
		assert.Equal(t, len(ps2), 1)
		assert.Equal(t, ps2[0].Position.Y, 1)
		assert.True(t, ps2[0].Nari, "MiniMode で先手だと Y:1 から成れる")
	}
}

func TestMovablePositions3(t *testing.T) {
	b := ban.NewMicroBan()
	p1 := player.New(1, false)

	pos, _ := b.NewPosition(1, 2)
	b.SetKoma(pos, koma.FU, p1)

	ps, e := MovablePositions(p1, b, pos)
	if assert.Nil(t, e, "MicroMode で後手の成りテスト") {
		assert.Equal(t, len(ps), 1)
		assert.Equal(t, ps[0].Position.Y, 3)
		assert.True(t, ps[0].Nari, "MicroMode で後手だと Y:3 から成れる")
	}

	p2 := player.New(2, true)

	pos2, _ := b.NewPosition(1, 1)
	b.SetKoma(pos2, koma.FU, p2)
	ps2, e := MovablePositions(p2, b, pos2)
	if assert.Nil(t, e, "MicroMode で先手の成テスト") {
		assert.Equal(t, len(ps2), 1)
		assert.Equal(t, ps2[0].Position.Y, 0)
		assert.True(t, ps2[0].Nari, "MicroMode で先手だと Y:0 から成れる")
	}
}

func TestFrontPositions(t *testing.T) {
	b := ban.NewDefaultBan()
	p1 := player.New(1, true)
	p2 := player.New(2, false)

	from, _ := b.NewPosition(4, 8)
	to, _ := b.NewPosition(4, 7)
	k := b.SetKoma(from, koma.GYOKU, p1)
	ps, e := FrontPositions(b, k, from)
	if assert.Nil(t, e, "前に移動できる") {
		assert.Equal(t, len(ps), 1)
		assert.True(t, to.Equal(ps[0].Position))
	}

	k2 := b.SetKoma(to, koma.FU, p1)
	ps, e = FrontPositions(b, k, from)
	assert.NotNil(t, e, "前に自分の駒があると移動できない")

	k2.Player = p2
	ps, e = FrontPositions(b, k, from)
	assert.Nil(t, e, "前にあるのが敵の駒なら移動できる")
}

func TestFrontFrontRightJumpPositions(t *testing.T) {
	b := ban.NewDefaultBan()
	p := player.New(1, true)

	from, _ := b.NewPosition(1, 8)
	to, _ := b.NewPosition(0, 6)
	k := b.SetKoma(from, koma.KEI, p)
	ps, e := FrontFrontRightJumpPositions(b, k, from)

	assert.Nil(t, e, "移動できる")
	assert.Equal(t, len(ps), 1, "ジャンプしているので途中のマスは候補に出ない")
	assert.True(t, to.Equal(ps[0].Position), "桂馬飛びできている")
}

func TestFrontStraightPositions(t *testing.T) {
	b := ban.NewDefaultBan()
	p := player.New(1, true)

	from, _ := b.NewPosition(1, 7)
	k := b.SetKoma(from, koma.HISYA, p)
	ps, e := FrontStraightPositions(b, k, from)

	assert.Nil(t, e, "移動できる")
	assert.Equal(t, len(ps), 7, "前方の全てのマスの候補がある")
	for i, pos := range []*MovePosition{
		&MovePosition{&ban.Position{1, 6}, false},
		&MovePosition{&ban.Position{1, 5}, false},
		&MovePosition{&ban.Position{1, 4}, false},
		&MovePosition{&ban.Position{1, 3}, false},
		&MovePosition{&ban.Position{1, 2}, false},
		&MovePosition{&ban.Position{1, 1}, false},
		&MovePosition{&ban.Position{1, 0}, false},
	} {
		assert.True(t, ps[i].Position.Equal(pos.Position), "前方のすべてのマスの候補がある")
	}

	p2 := player.New(2, false)
	b.SetKoma(&ban.Position{1, 1}, koma.FU, p2)
	ps, e = FrontStraightPositions(b, k, from)

	//for _, pos1 := range ps {
	//	t.Log(pos1)
	//}

	assert.Nil(t, e, "前方に敵の駒があっても移動できる")
	assert.Equal(t, len(ps), 6, "前方の敵の駒のマスまでの候補がある")
	for i, pos := range []*MovePosition{
		&MovePosition{&ban.Position{1, 6}, false},
		&MovePosition{&ban.Position{1, 5}, false},
		&MovePosition{&ban.Position{1, 4}, false},
		&MovePosition{&ban.Position{1, 3}, false},
		&MovePosition{&ban.Position{1, 2}, false},
		&MovePosition{&ban.Position{1, 1}, false},
	} {
		assert.True(t, ps[i].Position.Equal(pos.Position), "前方の敵の駒のマスまでの候補がある")
	}

	b.SetKoma(&ban.Position{1, 4}, koma.FU, p)
	ps, e = FrontStraightPositions(b, k, from)

	assert.Nil(t, e, "前方に自分の駒があっても移動できる")
	assert.Equal(t, len(ps), 2, "前方の自分の駒のマスの前までの候補がある")
	for i, pos := range []*MovePosition{
		&MovePosition{&ban.Position{1, 6}, false},
		&MovePosition{&ban.Position{1, 5}, false},
	} {
		assert.True(t, ps[i].Position.Equal(pos.Position), "前方の自分の駒のマスの前までの候補がある")
	}
}
