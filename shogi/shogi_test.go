package shogi

import (
	"bitbucket.org/dchiba/shogi/apptime"
	"bitbucket.org/dchiba/shogi/ban"
	"bitbucket.org/dchiba/shogi/koma"
	"bitbucket.org/dchiba/shogi/player"
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

func TestShogi_New(t *testing.T) {
	assert := assert.New(t)
	s := New(DefaultMode)
	assert.NotNil(s, "Shogi is not exist")
}

func TestShogi_Initialize(t *testing.T) {
	assert := assert.New(t)
	s := New(DefaultMode)
	p1, p2 := player.New(1, true), player.New(2, false)
	s.SetPlayer(p1)
	s.SetPlayer(p2)

	s.Initialize()
	assert.Equal(s.Ban.MaxID, uint8(40), "駒が並べられている")

	ps, e := s.MovablePositions(p1, &ban.Position{4, 6})
	if assert.Nil(e, "移動先候補が正常に取得できる") {
		assert.Equal(len(ps), 1, "歩が前のマスにすすめる")
		assert.True(ps[0].Position.Equal(&ban.Position{4, 5}), "歩が前のマスにすすめる")
	}

	now := time.Now()
	apptime.SetTime(now.Add(1 * time.Minute))
	moved, got, captured, e := s.Move(p1, &ban.Position{4, 6}, &ban.Position{4, 5}, false)
	if assert.Nil(e, "移動が正常にできる") {
		assert.Equal(moved.Type, koma.FU, "歩を動かした")
		assert.Nil(got)
	}

	apptime.SetTime(now.Add(2 * time.Minute))
	moved, got, captured, e = s.Move(p1, &ban.Position{4, 5}, &ban.Position{4, 4}, false)

	apptime.SetTime(now.Add(3 * time.Minute))
	moved, got, captured, e = s.Move(p1, &ban.Position{4, 4}, &ban.Position{4, 3}, false)

	apptime.SetTime(now.Add(4 * time.Minute))
	moved, got, captured, e = s.Move(p1, &ban.Position{4, 3}, &ban.Position{4, 2}, false)
	if assert.Nil(e, "敵陣に入る") {
		assert.Equal(moved.Type, koma.FU, "敵陣に入ったが成り指定をしなければ歩のまま")
		assert.Equal(got.Type, koma.FU, "相手の歩を取った")
		assert.Equal(captured.Type, koma.FU, "相手の歩を取った")
		assert.Equal(got.Player, s.Player2, "相手の歩を取った")
	}

	apptime.SetTime(now.Add(5 * time.Minute))
	moved, got, captured, e = s.Move(p1, &ban.Position{4, 2}, &ban.Position{4, 1}, true)
	assert.Equal(moved.Type, koma.TOKIN, "成り指定を行い成った")

	apptime.SetTime(now.Add(6 * time.Minute))
	moved, got, captured, e = s.Move(p1, &ban.Position{4, 1}, &ban.Position{4, 0}, false)
	if assert.Nil(e, "相手の玉を取る") {
		assert.Equal(got.Type, koma.GYOKU, "相手の玉を取った")
		t.Log(s)
		assert.Equal(s.Status, Finished, "終了")
		assert.Equal(s.Winner.ID, p1.ID, "先手の勝ち")
	}
}

func TestShogi_Initialize2(t *testing.T) {
	s := New(DefaultMode)
	assert.Panics(t, func() { s.Initialize() }, "プレイヤーがいないので異常終了")

	s.SetPlayer(player.New(1, true))
	assert.Panics(t, func() { s.Initialize() }, "プレイヤーがそろっていないので異常終了")

	s.SetPlayer(player.New(2, true))
	assert.NotPanics(t, func() { s.Initialize() }, "正常終了")

	assert.Panics(t, func() { s.Initialize() }, "実行済みなので異常終了")
}
