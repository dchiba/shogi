package play_kifu

import (
	"bitbucket.org/dchiba/shogi/ban"
	"bitbucket.org/dchiba/shogi/koma"
	"bitbucket.org/dchiba/shogi/player"
	"bitbucket.org/dchiba/shogi/shogi"
	"bitbucket.org/dchiba/shogi/util"
	"bufio"
	"log"
	"os"
	"strconv"
)

var (
	start_string = "+"
	end_string   = "%TORYO"
)

var typeMap = map[string]koma.Type{
	"FU": koma.FU,
	"KY": koma.KYOU,
	"KE": koma.KEI,
	"GI": koma.GIN,
	"KI": koma.KIN,
	"KA": koma.KAKU,
	"HI": koma.HISYA,
	"OU": koma.GYOKU,
	"TO": koma.TOKIN,
	"NY": koma.NARIKYOU,
	"NK": koma.NARIKEI,
	"NG": koma.NARIGIN,
	"UM": koma.UMA,
	"RY": koma.RYU,
}

func PlayKifu(name string) {
	fp, e := os.Open(name)
	if e != nil {
		log.Panic(e)
	}
	defer fp.Close()

	playing := false

	s := shogi.New(shogi.DefaultMode)
	s.SetPlayer(player.New(1, true))
	s.SetPlayer(player.New(2, false))
	s.Initialize()

	scanner := bufio.NewScanner(fp)
Loop:
	for scanner.Scan() {
		line := scanner.Text()
		if !playing && line == start_string {
			playing = true
			continue
		}
		switch {
		case line == end_string:
			break
		case line[0] == '+', line[0] == '-':
			log.Printf(line)

			sente := line[0] == '+'
			x1, _ := strconv.Atoi(string(line[1]))
			y1, _ := strconv.Atoi(string(line[2]))
			x2, _ := strconv.Atoi(string(line[3]))
			y2, _ := strconv.Atoi(string(line[4]))
			from := &ban.Position{X: x1 - 1, Y: y1 - 1}
			to := &ban.Position{X: x2 - 1, Y: y2 - 1}
			t := typeMap[line[5:]]

			p := util.Ternary(sente, s.Player1, s.Player2).(*player.Player)

			// 手駒を打つ
			if x1 == 0 && y1 == 0 {
				for _, k := range p.Tegoma {
					if k.Type == t {
						_, e := s.PutKoma(p, k, to)
						if e != nil {
							log.Panic(e)
						}
						continue Loop
					}
				}
				log.Panicf("%s is not found", t.Label())
			}

			// 駒を動かす
			k, e := s.Ban.GetKoma(from)
			if e != nil {
				log.Panic(e)
			}
			nari := false
			if t != k.Type {
				log.Println(line[5:])
				log.Println(t)
				log.Println(k)

				if nt, e := k.Type.Nari(); e != nil || t != nt {
					log.Print(s.Ban)
					log.Panic(e)
				}
				nari = true
			}

			_, _, _, e = s.Move(p, from, to, nari)
			if e != nil {
				log.Print(s.Ban)
				log.Panic(e)
			}
		}
	}
	if e := scanner.Err(); e != nil {
		log.Panic(e)
	}
}
