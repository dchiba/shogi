package play_kifu

import (
	"bitbucket.org/dchiba/shogi/koma"
	"io/ioutil"
	"testing"
)

var dir = "./files/"

func TestPlayKifu(t *testing.T) {

	// 移動待ち時間を無効にする
	koma.WaitTimeList = [...]int64{
		0,
		-1, -1, -1, -1, -1, -1, -1, -1,
		-1, -1, -1, -1, -1, -1, -1, -1,
	}

	files, e := ioutil.ReadDir(dir)
	if e != nil {
		t.Fatal(e)
	}

	for _, f := range files {
		name := dir + f.Name()
		t.Log(name)
		PlayKifu(name)
	}
}
